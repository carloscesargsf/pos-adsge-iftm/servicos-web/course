package com.educandoweb.course.resources;

import com.educandoweb.course.dtos.OrderDTO;
import com.educandoweb.course.dtos.OrderItemDTO;
import com.educandoweb.course.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderResource {

    @Autowired
    private OrderService orderService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<List<OrderDTO>> findAll() {
        return ResponseEntity.ok(orderService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

    @GetMapping("/{id}/items")
    public ResponseEntity<List<OrderItemDTO>> findItems(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.findItems(id));
    }

    @GetMapping("/myorders")
    public ResponseEntity<List<OrderDTO>> findByClient() {
        return ResponseEntity.ok(orderService.findByClient());
    }

    @GetMapping("/client/{clientId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<List<OrderDTO>> findByClientId(@PathVariable Long clientId) {
        return ResponseEntity.ok(orderService.findByClientId(clientId));
    }

    @PostMapping
    public ResponseEntity<OrderDTO> placeOrder(@RequestBody List<OrderItemDTO> orderItemDtos) {
        OrderDTO orderDTO = orderService.placeOrder(orderItemDtos);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(orderDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(orderDTO);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<OrderDTO> update(@PathVariable Long id, @RequestBody OrderDTO orderDTO) {
        return ResponseEntity.ok(orderService.update(id, orderDTO));
    }

}
