package com.educandoweb.course.resources;

import com.educandoweb.course.dtos.CategoryDTO;
import com.educandoweb.course.dtos.ProductCategoriesDTO;
import com.educandoweb.course.dtos.ProductDTO;
import com.educandoweb.course.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.data.domain.Pageable.unpaged;

@RestController
@RequestMapping("/products")
public class ProductResource {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<Page<ProductDTO>> findAll(@RequestParam(value = "name", defaultValue = "") String name,
                                                    @RequestParam(value = "categories", defaultValue = "") String categories,
                                                    @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                    @RequestParam(value = "linesPerPage", defaultValue = "12") Integer linesPerPage,
                                                    @RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
                                                    @RequestParam(value = "direction", defaultValue = "ASC") String direction) {
        Pageable pageRequest = linesPerPage > 0
                ? PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy)
                : unpaged();
        return ResponseEntity.ok(productService.findAll(name, categories, pageRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(productService.findById(id));
    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<Page<ProductDTO>> findByCategoryPaged(@PathVariable Long categoryId,
                                                                @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                                @RequestParam(value = "linesPerPage", defaultValue = "12") Integer linesPerPage,
                                                                @RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
                                                                @RequestParam(value = "direction", defaultValue = "ASC") String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
        return ResponseEntity.ok(productService.findByCategoryPaged(categoryId, pageRequest));
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<ProductDTO> insert(@Valid @RequestBody ProductCategoriesDTO productCategoriesDTO) {
        ProductDTO productDTO = productService.insert(productCategoriesDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productDTO.getId())
                .toUri();

        return ResponseEntity.created(uri).body(productDTO);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<ProductDTO> update(@PathVariable Long id, @RequestBody ProductCategoriesDTO productCategoriesDTO) {
        return ResponseEntity.ok(productService.update(id, productCategoriesDTO));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/addCategory")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Void> addCategory(@PathVariable Long id, @RequestBody CategoryDTO categoryDTO) {
        productService.addCategory(id, categoryDTO);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/removeCategory")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Void> removeCategory(@PathVariable Long id, @RequestBody CategoryDTO categoryDTO) {
        productService.removeCategory(id, categoryDTO);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/setCategories")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Void> setCategories(@PathVariable Long id, @RequestBody List<CategoryDTO> categoryDTOS) {
        productService.setCategories(id, categoryDTOS);
        return ResponseEntity.noContent().build();
    }

}
