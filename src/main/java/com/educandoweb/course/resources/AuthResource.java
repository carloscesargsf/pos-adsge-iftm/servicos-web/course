package com.educandoweb.course.resources;

import com.educandoweb.course.dtos.CredentialsDTO;
import com.educandoweb.course.dtos.EmailDTO;
import com.educandoweb.course.dtos.TokenDTO;
import com.educandoweb.course.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthResource {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> login(@RequestBody CredentialsDTO credentialsDTO) {
        return ResponseEntity.ok(authService.authenticate(credentialsDTO));
    }

    @PostMapping("/refresh")
    public ResponseEntity<TokenDTO> refresh() {
        return ResponseEntity.ok(authService.refreshToken());
    }

    @PostMapping("/forgot")
    public ResponseEntity<Void> forgot(@RequestBody EmailDTO emailDTO) {
        authService.sendNewPassword(emailDTO.getEmail());

        return ResponseEntity.noContent().build();
    }

}
