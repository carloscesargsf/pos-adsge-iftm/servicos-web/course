package com.educandoweb.course.repositories;

import com.educandoweb.course.entities.Category;
import com.educandoweb.course.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Transactional(readOnly = true)
    @Query("SELECT product FROM Product product " +
            "INNER JOIN product.categories categories " +
            "WHERE :category IN categories")
    Page<Product> findByCategory(@Param("category") Category category, Pageable pageable);

    @Transactional(readOnly = true)
    @Query("SELECT product FROM Product product " +
            "WHERE LOWER(product.name) LIKE CONCAT('%', LOWER(:name), '%')")
    Page<Product> findByNameContainingIgnoreCase(@Param("name") String name, Pageable pageable);

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT product FROM Product product " +
            "INNER JOIN product.categories categories " +
            "WHERE LOWER(product.name) LIKE CONCAT('%', LOWER(:name), '%') AND categories IN :categories")
    Page<Product> findByNameContainingIgnoreCaseAndCategoriesIn(@Param("name") String name,
                                                                @Param("categories") List<Category> categories,
                                                                Pageable pageable);

}
