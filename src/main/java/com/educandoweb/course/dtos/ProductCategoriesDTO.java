package com.educandoweb.course.dtos;

import com.educandoweb.course.entities.Product;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductCategoriesDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "can't be empty")
    @Length(min = 3, max = 80, message = "length must be between 3 and 80")
    private String name;

    @NotEmpty(message = "can't be empty")
    @Length(min = 8, message = "length must be greater than or equal to 8")
    private String description;

    @Positive
    private Double price;

    private String imgUrl;

    private List<CategoryDTO> categoriesDTOS = new ArrayList<>();

    public ProductCategoriesDTO() {
    }

    public ProductCategoriesDTO(String name, String description, Double price, String imgUrl) {
        setName(name);
        setDescription(description);
        setPrice(price);
        setImgUrl(imgUrl);
    }

    public ProductCategoriesDTO(Product product) {
        setName(product.getName());
        setDescription(product.getDescription());
        setPrice(product.getPrice());
        setImgUrl(product.getImgUrl());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<CategoryDTO> getCategoriesDTOS() {
        return categoriesDTOS;
    }

    public Product toEntity() {
        return new Product(null, getName(), getDescription(), getPrice(), getImgUrl());
    }

}
