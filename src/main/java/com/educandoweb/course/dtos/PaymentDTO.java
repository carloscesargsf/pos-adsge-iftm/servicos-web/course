package com.educandoweb.course.dtos;

import com.educandoweb.course.entities.Order;
import com.educandoweb.course.entities.Payment;

import java.io.Serializable;
import java.time.Instant;

public class PaymentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Instant moment;

    private Long orderId;

    public PaymentDTO() {
    }

    public PaymentDTO(Payment payment) {
        setId(payment.getId());
        setMoment(payment.getMoment());
        setOrderId(payment.getOrder().getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getMoment() {
        return moment;
    }

    public void setMoment(Instant moment) {
        this.moment = moment;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Payment toEntity() {
        Order order = new Order(getOrderId(), null, null, null);
        return new Payment(getId(), getMoment(), order);
    }

}
