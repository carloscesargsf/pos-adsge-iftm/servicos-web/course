package com.educandoweb.course.dtos;

import com.educandoweb.course.entities.Order;
import com.educandoweb.course.entities.User;
import com.educandoweb.course.entities.enums.OrderStatus;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.Instant;

public class OrderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "GMT")
    private Instant moment;

    private OrderStatus orderStatus;

    private Long clientId;

    private String clientName;

    private String clientEmail;

    public OrderDTO() {
    }

    public OrderDTO(Long id, Instant moment, OrderStatus orderStatus, Long clientId, String clientName, String clientEmail) {
        setId(id);
        setMoment(moment);
        setOrderStatus(orderStatus);
        setClientId(clientId);
        setClientName(clientName);
        setClientEmail(clientEmail);
    }

    public OrderDTO(Order order) {
        if (order.getClient() == null) {
            throw new IllegalArgumentException("Error instantiating OrderDTO: client was null.");
        }

        setId(order.getId());
        setMoment(order.getMoment());
        setOrderStatus(order.getOrderStatus());
        setClientId(order.getClient().getId());
        setClientName(order.getClient().getName());
        setClientEmail(order.getClient().getEmail());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getMoment() {
        return moment;
    }

    public void setMoment(Instant moment) {
        this.moment = moment;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public Order toEntity() {
        User client = new User(getClientId(), getClientName(), getClientEmail(), null, null);

        return new Order(getId(), getMoment(), getOrderStatus(), client);
    }

}
