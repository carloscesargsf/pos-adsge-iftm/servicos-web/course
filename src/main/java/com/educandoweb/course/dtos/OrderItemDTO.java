package com.educandoweb.course.dtos;

import com.educandoweb.course.entities.OrderItem;

import java.io.Serializable;

public class OrderItemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer quantity;

    private Double price;

    private Long productId;

    private String productName;

    private String productImgUrl;

    public OrderItemDTO() {
    }

    public OrderItemDTO(OrderItem orderItem) {
        if (orderItem == null) {
            throw new IllegalArgumentException("Product was null");
        }

        setQuantity(orderItem.getQuantity());
        setPrice(orderItem.getPrice());
        setProductId(orderItem.getProduct().getId());
        setProductName(orderItem.getProduct().getName());
        setProductImgUrl(orderItem.getProduct().getImgUrl());
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }
}
