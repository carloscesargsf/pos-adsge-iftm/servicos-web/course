package com.educandoweb.course.dtos;

import com.educandoweb.course.entities.User;
import com.educandoweb.course.services.validations.UserInsertValid;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@UserInsertValid
public class UserInsertDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    public Long id;

    @NotEmpty(message = "can't be empty")
    @Length(min = 5, max = 80, message = "length must be between 5 and 80")
    public String name;

    @NotEmpty(message = "can't be empty")
    @Email(message = "invalid e-mail")
    public String email;

    @NotEmpty(message = "can't be empty")
    @Length(min = 8, max = 20, message = "length must be between 8 and 20")
    public String phone;

    @NotEmpty(message = "can't be empty")
    public String password;

    public UserInsertDTO() {
    }

    public UserInsertDTO(User user) {
        setId(user.getId());
        setName(user.getName());
        setEmail(user.getEmail());
        setPhone(user.getPhone());
        setPassword(user.getPassword());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User toEntity() {
        return new User(getId(), getName(), getEmail(), getPhone(), getPassword());
    }

}
