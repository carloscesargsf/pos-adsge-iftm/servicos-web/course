package com.educandoweb.course.services;

import com.educandoweb.course.dtos.CredentialsDTO;
import com.educandoweb.course.dtos.TokenDTO;
import com.educandoweb.course.entities.Order;
import com.educandoweb.course.entities.User;
import com.educandoweb.course.repositories.UserRepository;
import com.educandoweb.course.security.JWTUtil;
import com.educandoweb.course.services.exceptions.JWTAuthenticationException;
import com.educandoweb.course.services.exceptions.JWTAuthorizationException;
import com.educandoweb.course.services.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
public class AuthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public TokenDTO authenticate(CredentialsDTO credentialsDTO) {
        try {
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    credentialsDTO.getEmail(), credentialsDTO.getPassword());
            authenticationManager.authenticate(authToken);

            return generateTokenDTO(credentialsDTO.getEmail());
        } catch (AuthenticationException e) {
            throw new JWTAuthenticationException("Bad credentials");
        }
    }

    public User authenticated() {
        try {
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal();

            return userRepository.findByEmail(userDetails.getUsername());
        } catch (Exception e) {
            throw new JWTAuthorizationException("Access denied");
        }
    }

    public void validateSelfOrAdmin(Long userId) {
        User user = authenticated();

        if (user == null || (!user.getId().equals(userId) && !user.hasRole("ROLE_ADMIN"))) {
            throw new JWTAuthorizationException("Access denied");
        }
    }

    public void validateOwnOrderOrAdmin(Order order) {
        User user = authenticated();

        if (user == null || (!user.getId().equals(order.getClient().getId()) && !user.hasRole("ROLE_ADMIN"))) {
            throw new JWTAuthorizationException("Access denied");
        }
    }

    public TokenDTO refreshToken() {
        User user = authenticated();

        return generateTokenDTO(user.getEmail());
    }

    public void sendNewPassword(String email) {
        User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new ResourceNotFoundException("Email not found");
        }

        String newPassword = newPassword();

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);

        LOGGER.info("New password: " + newPassword);
    }

    private TokenDTO generateTokenDTO(String email) {
        return new TokenDTO(email, jwtUtil.generateToken(email));
    }

    private String newPassword() {
        char[] vector = new char[10];

        for (int i = 0; i < 10; i++) {
            vector[i] = randomChar();
        }

        return new String(vector);
    }

    private char randomChar() {
        Random random = new Random();

        int option = random.nextInt(3);
        if (option == 0) {
            return (char) (random.nextInt(10) + 48);
        } else if (option == 1) {
            return (char) (random.nextInt(26) + 65);
        } else {
            return (char) (random.nextInt(26) + 97);
        }
    }

}
