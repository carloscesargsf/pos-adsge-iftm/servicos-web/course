package com.educandoweb.course.services;

import com.educandoweb.course.dtos.OrderDTO;
import com.educandoweb.course.dtos.OrderItemDTO;
import com.educandoweb.course.entities.Order;
import com.educandoweb.course.entities.OrderItem;
import com.educandoweb.course.entities.Product;
import com.educandoweb.course.entities.User;
import com.educandoweb.course.entities.enums.OrderStatus;
import com.educandoweb.course.repositories.OrderItemRepository;
import com.educandoweb.course.repositories.OrderRepository;
import com.educandoweb.course.repositories.ProductRepository;
import com.educandoweb.course.repositories.UserRepository;
import com.educandoweb.course.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    public List<OrderDTO> findAll() {
        return orderRepository.findAll()
                .stream()
                .map(e -> new OrderDTO(e))
                .collect(Collectors.toList());
    }

    public OrderDTO findById(Long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));
        authService.validateOwnOrderOrAdmin(order);

        return new OrderDTO(order);
    }

    public List<OrderDTO> findByClient() {
        User client = authService.authenticated();

        return orderRepository.findByClient(client)
                .stream()
                .map(e -> new OrderDTO(e))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<OrderItemDTO> findItems(Long id) {
        Order order = orderRepository.getOne(id);
        authService.validateOwnOrderOrAdmin(order);

        return order.getItems()
                .stream()
                .map(e -> new OrderItemDTO(e))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<OrderDTO> findByClientId(Long clientId) {
        User client = userRepository.getOne(clientId);

        return orderRepository.findByClient(client)
                .stream()
                .map(e -> new OrderDTO(e))
                .collect(Collectors.toList());
    }

    @Transactional
    public OrderDTO placeOrder(List<OrderItemDTO> orderItemDtos) {
        User client = authService.authenticated();
        Order order = new Order(null, Instant.now(), OrderStatus.WAITING_PAYMENT, client);

        for (OrderItemDTO orderItemDTO : orderItemDtos) {
            Product product = productRepository.getOne(orderItemDTO.getProductId());
            OrderItem orderItem = new OrderItem(order, product, orderItemDTO.getQuantity(), orderItemDTO.getPrice());
            order.getItems().add(orderItem);
        }

        orderRepository.save(order);
        orderItemRepository.saveAll(order.getItems());

        return new OrderDTO(order);
    }

    @Transactional
    public OrderDTO update(Long id, OrderDTO orderDTO) {
        try {
            Order orderDB = orderRepository.getOne(id);
            updateData(orderDB, orderDTO);
            return new OrderDTO(orderRepository.save(orderDB));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    private void updateData(Order order, OrderDTO orderDTO) {
        order.setOrderStatus(orderDTO.getOrderStatus());
    }


}
