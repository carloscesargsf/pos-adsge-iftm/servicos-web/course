package com.educandoweb.course.services;

import com.educandoweb.course.dtos.CategoryDTO;
import com.educandoweb.course.dtos.ProductCategoriesDTO;
import com.educandoweb.course.dtos.ProductDTO;
import com.educandoweb.course.entities.Category;
import com.educandoweb.course.entities.Product;
import com.educandoweb.course.repositories.CategoryRepository;
import com.educandoweb.course.repositories.ProductRepository;
import com.educandoweb.course.services.exceptions.DatabaseException;
import com.educandoweb.course.services.exceptions.ParamFormatException;
import com.educandoweb.course.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public Page<ProductDTO> findAll(String name, String categories, Pageable pageable) {
        Page<Product> products;

        if (categories.equals("")) {
            products = productRepository.findByNameContainingIgnoreCase(name, pageable);
        } else {
            List<Category> categoriesList = parseIds(categories)
                    .stream()
                    .map(id -> categoryRepository.getOne(id))
                    .collect(Collectors.toList());

            products = productRepository.findByNameContainingIgnoreCaseAndCategoriesIn(name, categoriesList, pageable);
        }

        return products
                .map(e -> new ProductDTO(e));
    }

    public ProductDTO findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        return new ProductDTO(product.orElseThrow(() -> new ResourceNotFoundException(id)));
    }

    @Transactional
    public ProductDTO insert(ProductCategoriesDTO productCategoriesDTO) {
        Product product = productCategoriesDTO.toEntity();

        setProductCategories(product, productCategoriesDTO.getCategoriesDTOS());
        return new ProductDTO(productRepository.save(product));
    }

    @Transactional
    public ProductDTO update(Long id, ProductCategoriesDTO productCategoriesDTO) {
        try {
            Product productDB = productRepository.getOne(id);
            updateData(productDB, productCategoriesDTO);
            return new ProductDTO(productRepository.save(productDB));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    public void delete(Long id) {
        try {
            productRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

    private void updateData(Product productDB, ProductCategoriesDTO productCategoriesDTO) {
        productDB.setName(productCategoriesDTO.getName());
        productDB.setDescription(productCategoriesDTO.getDescription());
        productDB.setPrice(productCategoriesDTO.getPrice());
        productDB.setImgUrl(productCategoriesDTO.getImgUrl());

        if (productCategoriesDTO.getCategoriesDTOS() != null && productCategoriesDTO.getCategoriesDTOS().size() > 0) {
            setProductCategories(productDB, productCategoriesDTO.getCategoriesDTOS());
        }
    }

    private void setProductCategories(Product product, List<CategoryDTO> categoryDTOS) {
        product.getCategories().clear();

        categoryDTOS.stream()
                .map(categoryDTO -> product.getCategories().add(categoryRepository.getOne(categoryDTO.getId())))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Page<ProductDTO> findByCategoryPaged(Long categoryId, Pageable pageable) {
        Category category = categoryRepository.getOne(categoryId);

        return productRepository.findByCategory(category, pageable)
                .map(e -> new ProductDTO(e));
    }

    @Transactional
    public void addCategory(Long productId, CategoryDTO categoryDTO) {
        Product product = productRepository.getOne(productId);
        product.getCategories().add(categoryRepository.getOne(categoryDTO.getId()));

        productRepository.save(product);
    }

    @Transactional
    public void removeCategory(Long productId, CategoryDTO categoryDTO) {
        Product product = productRepository.getOne(productId);
        product.getCategories().remove(categoryRepository.getOne(categoryDTO.getId()));

        productRepository.save(product);
    }

    @Transactional
    public void setCategories(Long productId, List<CategoryDTO> categoryDTOS) {
        Product product = productRepository.getOne(productId);
        setProductCategories(product, categoryDTOS);

        productRepository.save(product);
    }

    private List<Long> parseIds(String categories) {
        List<Long> categoriesIds = new ArrayList<>();

        for (String categoryId : categories.split(",")) {
            try {
                categoriesIds.add(Long.parseLong(categoryId));
            } catch (NumberFormatException e) {
                throw new ParamFormatException("Invalid categories format");
            }
        }

        return categoriesIds;
    }

}
