package com.educandoweb.course.services;

import com.educandoweb.course.dtos.PaymentDTO;
import com.educandoweb.course.entities.Order;
import com.educandoweb.course.entities.Payment;
import com.educandoweb.course.repositories.OrderRepository;
import com.educandoweb.course.repositories.PaymentRepository;
import com.educandoweb.course.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private OrderRepository orderRepository;

    public List<PaymentDTO> findAll() {
        return paymentRepository.findAll()
                .stream()
                .map(e -> new PaymentDTO(e))
                .collect(Collectors.toList());
    }

    public PaymentDTO findById(Long id) {
        Optional<Payment> payment = paymentRepository.findById(id);
        return new PaymentDTO(payment.orElseThrow(() -> new ResourceNotFoundException(id)));
    }

    @Transactional
    public PaymentDTO insert(PaymentDTO paymentDTO) {
        Order order = orderRepository.getOne(paymentDTO.getOrderId());
        Payment payment = new Payment(null, paymentDTO.getMoment(), order);
        order.setPayment(payment);
        orderRepository.save(order);

        return new PaymentDTO(order.getPayment());
    }

    @Transactional
    public PaymentDTO update(Long id, PaymentDTO paymentDTO) {
        try {
            Payment paymentDB = paymentRepository.getOne(id);
            updateData(paymentDB, paymentDTO);
            return new PaymentDTO(paymentRepository.save(paymentDB));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(id);
        }
    }

    private void updateData(Payment paymentDB, PaymentDTO paymentDTO) {
        paymentDB.setMoment(paymentDTO.getMoment());
    }

}
